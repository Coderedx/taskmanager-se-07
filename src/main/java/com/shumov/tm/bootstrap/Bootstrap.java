package com.shumov.tm.bootstrap;


import com.shumov.tm.api.service.IProjectService;
import com.shumov.tm.api.service.ITaskService;
import com.shumov.tm.api.service.IUserService;
import com.shumov.tm.api.service.ServiceLocator;
import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.command.entity.user.*;
import com.shumov.tm.command.help.AboutCommand;
import com.shumov.tm.command.help.ExitCommand;
import com.shumov.tm.command.help.HelpCommand;
import com.shumov.tm.command.entity.project.*;
import com.shumov.tm.command.entity.task.*;
import com.shumov.tm.entity.User;
import com.shumov.tm.exception.command.CommandCorruptException;
import com.shumov.tm.exception.command.CommandWrongException;
import com.shumov.tm.repository.ProjectRepository;
import com.shumov.tm.repository.TaskRepository;
import com.shumov.tm.repository.UserRepository;
import com.shumov.tm.service.ProjectService;
import com.shumov.tm.service.TaskService;
import com.shumov.tm.service.UserService;
import com.shumov.tm.util.constant.UserRoleType;

import java.io.IOException;
import java.util.*;

public class Bootstrap implements ServiceLocator {

    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    private final IProjectService projectService = new ProjectService(new ProjectRepository());
    private final ITaskService taskService = new TaskService(new TaskRepository());
    private final IUserService userService = new UserService(new UserRepository());
    private final Scanner terminalService = new Scanner(System.in);
    private User guestUser = new User("guest","guest", UserRoleType.GUEST);
    private User currentUser = guestUser;


    public Bootstrap() { }

    public void init() throws Exception {
        AbstractCommand[] commandsInit = {
                new ProjectAdminClearDBCommand(),
                new ProjectAdminEditCommand(),
                new ProjectAdminListCommand(),
                new ProjectAdminRemoveCommand(),
                new ProjectAdminTasksCommand(),
                new TaskAdminClearDBCommand(),
                new TaskAdminAddProjectCommand(),
                new TaskAdminEditCommand(),
                new TaskAdminRemoveCommand(),
                new TaskAdminListCommand(),
                new ProjectClearCommand(),
                new ProjectCreateCommand(),
                new ProjectEditCommand(),
                new ProjectListCommand(),
                new ProjectRemoveCommand(),
                new ProjectTasksCommand(),
                new TaskAddProjectCommand(),
                new TaskClearCommand(),
                new TaskCreateCommand(),
                new TaskEditCommand(),
                new TaskListCommand(),
                new TaskRemoveCommand(),
                new UserRegAdminCommand(),
                new UserAdminReviewCommand(),
                new UserLoginCommand(),
                new UserRegCommand(),
                new UserCloseSessionCommand(),
                new UserReviewCommand(),
                new UserEditCommand(),
                new UserUpdPassCommand(),
                new HelpCommand(),
                new AboutCommand(),
                new ExitCommand()};
        commands.clear();
        initUsers();
        for (AbstractCommand abstractCommand : commandsInit) {
            try {
                registry(abstractCommand);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        start();
    }

    private void registry(final AbstractCommand command) throws CommandCorruptException {
        final String cliCommand = command.command();
        final String cliDescription = command.getDescription();
        if (cliCommand == null || cliCommand.isEmpty()) {
            throw new CommandCorruptException();
        }
        if (cliDescription == null || cliDescription.isEmpty()) {
            throw new CommandCorruptException();
        }
        command.setServiceLocator(this);
        commands.put(command.command(), command);
    }

    private void start() {
        System.out.println("[WELCOME TO TASK MANAGER]");
        System.out.println("[ENTER \"help\" TO GET COMMAND LIST]");
        String command = "";
        while (!"exit".equals(command)) {
            System.out.println("\nENTER COMMAND:");
            command = terminalService.nextLine();
            try {
                execute(command);
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
    }

    private void execute(final String command) throws Exception {
        if (command == null || command.isEmpty()) {
            throw new CommandWrongException();
        }
        final AbstractCommand abstractCommand = commands.get(command);
        if (abstractCommand == null) {
            throw new CommandWrongException();
        }
        // Проверка валидности выполнения операции.
        if(abstractCommand.isSecure()){
        abstractCommand.execute();
        } else {
            throw new IOException("access error!".toUpperCase());
        }
    }

    private void initUsers() throws Exception {
        try {
            userService.createNewUser("user", "user", UserRoleType.USER);
            userService.createNewUser("admin", "admin", UserRoleType.ADMIN);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }

    public Scanner getTerminalService() {
        return terminalService;
    }

    public IProjectService getProjectService() {
        return projectService;
    }

    public ITaskService getTaskService() {
        return taskService;
    }

    public IUserService getUserService() {
        return userService;
    }

    public List<AbstractCommand> getCommands() {
        return new ArrayList<>(commands.values());
    }

    public User getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(User currentUser) {
        this.currentUser = currentUser;
    }

    public User getGuestUser() {
        return guestUser;
    }
}

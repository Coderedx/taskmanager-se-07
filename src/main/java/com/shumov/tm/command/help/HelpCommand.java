package com.shumov.tm.command.help;

import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.entity.User;
import com.shumov.tm.util.constant.UserRoleType;

import java.util.ArrayList;
import java.util.List;

public final class HelpCommand extends AbstractCommand {

    @Override
    public String command() {
        return "help";
    }

    @Override
    public String getDescription() {
        return "Show all commands";
    }

    @Override
    public void execute() throws Exception {
        for (AbstractCommand command : checkCommandsList()){
            System.out.println(command.command()+": "+command.getDescription());
        }
    }

    private List<AbstractCommand> checkCommandsList(){
        final List<AbstractCommand> list = new ArrayList<>();
        final UserRoleType role = serviceLocator.getCurrentUser().getUserRoleType();
        for (AbstractCommand abstractCommand : serviceLocator.getCommands()){
            if(abstractCommand.getRoleTypes().contains(role)){
                list.add(abstractCommand);
            }
        }
        return list;
    }

    @Override
    protected void init() {
        roleTypes.add(UserRoleType.ADMIN);
        roleTypes.add(UserRoleType.USER);
        roleTypes.add(UserRoleType.GUEST);
    }
}

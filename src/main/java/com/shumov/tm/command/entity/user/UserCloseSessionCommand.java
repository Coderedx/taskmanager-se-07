package com.shumov.tm.command.entity.user;

import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.util.constant.UserRoleType;

public class UserCloseSessionCommand extends AbstractCommand {

    @Override
    public String command() {
        return "user-close";
    }

    @Override
    public String getDescription() {
        return "End current session";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[END CURRENT SESSION]");
        System.out.println("[Enter \"Y\" if you confirm]".toUpperCase());
        final String confirm = serviceLocator.getTerminalService().nextLine();
        if(!"y".equals(confirm.toLowerCase())){
            System.out.println("[operation has not been confirmed]".toUpperCase());
            return;
        }
        serviceLocator.setCurrentUser(serviceLocator.getGuestUser());
        System.out.println("Current session completed successfully".toUpperCase());
    }

    @Override
    protected void init() {
        roleTypes.add(UserRoleType.USER);
        roleTypes.add(UserRoleType.ADMIN);
    }
}

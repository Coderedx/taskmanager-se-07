package com.shumov.tm.command.entity.user;

import com.shumov.tm.api.service.IUserService;
import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.entity.User;
import com.shumov.tm.util.constant.UserRoleType;

import java.util.Scanner;

public class UserRegCommand extends AbstractCommand {

    @Override
    public String command() {
        return "user-reg";
    }

    @Override
    public String getDescription() {
        return "New user registration";
    }

    @Override
    public void execute() throws Exception {
        createUser();
    }

    private void createUser() throws Exception  {
        System.out.println("[NEW USER REGISTRATION]");
        final IUserService userService = serviceLocator.getUserService();
        System.out.println("[ENTER LOGIN FOR NEW USER]");
        final String login = serviceLocator.getTerminalService().nextLine();
        userService.isWrongLogin(login);
        System.out.println("[ENTER PASSWORD FOR NEW USER]");
        final String pass = serviceLocator.getTerminalService().nextLine();
        userService.isWrongPass(pass);
        userService.createNewUser(login, pass);
        System.out.println("NEW USER CREATED SUCCESSFULLY");
    }

    @Override
    protected void init() {
        roleTypes.add(UserRoleType.ADMIN);
        roleTypes.add(UserRoleType.GUEST);
    }
}

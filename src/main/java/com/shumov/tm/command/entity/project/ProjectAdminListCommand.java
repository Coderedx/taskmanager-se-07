package com.shumov.tm.command.entity.project;

import com.shumov.tm.api.service.IProjectService;
import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.entity.Project;
import com.shumov.tm.util.constant.UserRoleType;

public class ProjectAdminListCommand extends AbstractCommand {

    @Override
    public String command() {
        return "project-list-admin";
    }

    @Override
    public String getDescription() {
        return "Show all projects of all users";
    }

    @Override
    public void execute() throws Exception {
        adminExecute();
    }

    @Override
    protected void init() {
        roleTypes.add(UserRoleType.ADMIN);
    }

    private void adminExecute() throws Exception {
        System.out.println("[PROJECT LIST]");
        final IProjectService projectService = serviceLocator.getProjectService();
        for (Project project : projectService.getProjectList()) {
            System.out.println("PROJECT ID: " + project.getId() +"\n"
                    + "PROJECT NAME: " + project.getName() + "\n" +
                    "USER ID: " + project.getOwnerId()+ "\n");
        }
    }
}

package com.shumov.tm.command.entity.user;

import com.shumov.tm.api.service.IUserService;
import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.entity.User;
import com.shumov.tm.util.constant.UserRoleType;

public class UserEditCommand extends AbstractCommand {

    @Override
    public String command() {
        return "user-edit";
    }

    @Override
    public String getDescription() {
        return "Edit user description";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[EDIT USER DESCRIPTION]");
        System.out.println("ENTER NEW DESCRIPTION:");
        final String description = serviceLocator.getTerminalService().nextLine();
        final IUserService userService = serviceLocator.getUserService();
        userService.isWrongDescription(description);
        final User user = serviceLocator.getCurrentUser();
        user.setDescription(description);
        userService.mergeUser(user);
        System.out.println("Description changed successfully".toUpperCase());
    }

    @Override
    protected void init() {
        roleTypes.add(UserRoleType.USER);
        roleTypes.add(UserRoleType.ADMIN);
    }
}

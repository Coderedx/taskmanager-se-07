package com.shumov.tm.command.entity.task;

import com.shumov.tm.api.service.ITaskService;
import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.util.constant.UserRoleType;

public class TaskAdminRemoveCommand extends AbstractCommand {
    @Override
    public String command() {
        return "task-remove-admin";
    }

    @Override
    public String getDescription() {
        return "Remove any task by id";
    }

    @Override
    public void execute() throws Exception {
        adminExecute();
    }

    @Override
    protected void init() {
        roleTypes.add(UserRoleType.ADMIN);
    }

    private void adminExecute() throws Exception {
        System.out.println("[REMOVE TASK BY ID]");
        System.out.println("ENTER TASK ID:");
        final String taskId = serviceLocator.getTerminalService().nextLine();
        final ITaskService taskService = serviceLocator.getTaskService();
        taskService.isWrongId(taskId);
        taskService.removeTaskById(taskId);
        System.out.println("TASK HAS BEEN REMOVED SUCCESSFULLY");
    }
}

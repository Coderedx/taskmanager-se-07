package com.shumov.tm.command.entity.task;

import com.shumov.tm.api.service.ITaskService;
import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.entity.Task;
import com.shumov.tm.util.constant.UserRoleType;

public class TaskAdminListCommand extends AbstractCommand {

    @Override
    public String command() {
        return "task-list-admin";
    }

    @Override
    public String getDescription() {
        return "Show all tasks of all users";
    }

    @Override
    public void execute() throws Exception {
        adminExecute();
    }

    @Override
    protected void init() {
        roleTypes.add(UserRoleType.ADMIN);
    }

    private void adminExecute() throws Exception {
        System.out.println("[TASK LIST]");
        final ITaskService taskService = serviceLocator.getTaskService();
        for (Task task : taskService.getTaskList()) {
            System.out.println("TASK ID: " + task.getId() + "\nTASK NAME: " + task.getName()
                    + "\nPROJECT ID: "+ task.getIdProject()
                    + "\nUSER ID: "+ task.getOwnerId()+"\n");
        }
    }
}

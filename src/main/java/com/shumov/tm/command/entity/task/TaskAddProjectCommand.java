package com.shumov.tm.command.entity.task;

import com.shumov.tm.api.service.IProjectService;
import com.shumov.tm.api.service.ITaskService;
import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.entity.Task;
import com.shumov.tm.entity.User;
import com.shumov.tm.util.constant.UserRoleType;

public class TaskAddProjectCommand extends AbstractCommand {

    @Override
    public String command() {
        return "task-add";
    }

    @Override
    public String getDescription() {
        return "Add task in project";
    }

    @Override
    public void execute() throws Exception {
        userExecute();
    }

    @Override
    protected void init() {
        roleTypes.add(UserRoleType.USER);
        roleTypes.add(UserRoleType.ADMIN);
    }

    private void userExecute() throws Exception {
        System.out.println("[ADD TASK IN PROJECT]");
        System.out.println("ENTER PROJECT ID:");
        final String projectId = serviceLocator.getTerminalService().nextLine();
        final IProjectService projectService = serviceLocator.getProjectService();
        projectService.isWrongId(projectId);
        final User user = serviceLocator.getCurrentUser();
        projectService.getProject(user.getId(), projectId);
        System.out.println("ENTER TASK ID:");
        final String taskId = serviceLocator.getTerminalService().nextLine();
        final ITaskService taskService = serviceLocator.getTaskService();
        taskService.isWrongId(taskId);
        final Task task = taskService.getTask(user.getId(), taskId);
        task.setIdProject(projectId);
        taskService.addTaskInProject(user.getId(),taskId,task);
        System.out.println("TASK HAS BEEN ADD SUCCESSFULLY");
    }
}

package com.shumov.tm.command.entity.project;

import com.shumov.tm.api.service.IProjectService;
import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.util.constant.UserRoleType;

public class ProjectAdminEditCommand extends AbstractCommand {

    @Override
    public String command() {
        return "project-edit-admin";
    }

    @Override
    public String getDescription() {
        return "Change name of any project";
    }

    @Override
    public void execute() throws Exception {
        adminExecute();
    }

    @Override
    protected void init() {
        roleTypes.add(UserRoleType.ADMIN);
    }

    private void adminExecute() throws Exception {
        System.out.println("[EDIT PROJECT NAME BY ID]");
        System.out.println("ENTER PROJECT ID:");
        final String id = serviceLocator.getTerminalService().nextLine();
        final IProjectService projectService = serviceLocator.getProjectService();
        projectService.isWrongId(id);
        System.out.println("ENTER NEW PROJECT NAME:");
        final String name = serviceLocator.getTerminalService().nextLine();
        projectService.isWrongName(name);
        projectService.editProjectNameById(id,name);
        System.out.println("PROJECT NAME HAS BEEN CHANGED SUCCESSFULLY");
    }
}

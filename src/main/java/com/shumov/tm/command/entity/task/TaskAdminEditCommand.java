package com.shumov.tm.command.entity.task;

import com.shumov.tm.api.service.ITaskService;
import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.util.constant.UserRoleType;

public class TaskAdminEditCommand extends AbstractCommand {

    @Override
    public String command() {
        return "task-edit-admin";
    }

    @Override
    public String getDescription() {
        return "Change name of any task";
    }

    @Override
    public void execute() throws Exception {
        adminExecute();
    }

    @Override
    protected void init() {
        roleTypes.add(UserRoleType.ADMIN);
    }

    private void adminExecute() throws Exception {
        System.out.println("[EDIT TASK NAME BY ID]");
        System.out.println("ENTER TASK ID:");
        final String taskId = serviceLocator.getTerminalService().nextLine();
        final ITaskService taskService = serviceLocator.getTaskService();
        taskService.isWrongId(taskId);
        System.out.println("ENTER NEW TASK NAME:");
        final String taskName = serviceLocator.getTerminalService().nextLine();
        taskService.isWrongName(taskName);
        taskService.editTaskNameById(taskId,taskName);
        System.out.println("TASK NAME HAS BEEN CHANGED SUCCESSFULLY");
    }
}

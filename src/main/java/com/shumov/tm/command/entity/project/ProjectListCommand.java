package com.shumov.tm.command.entity.project;

import com.shumov.tm.api.service.IProjectService;
import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.entity.Project;
import com.shumov.tm.entity.User;
import com.shumov.tm.util.constant.UserRoleType;

public class ProjectListCommand extends AbstractCommand {

    @Override
    public String command() {
        return "project-list";
    }

    @Override
    public String getDescription() {
        return "Show all projects";
    }

    @Override
    public void execute() throws Exception {
        userExecute();
    }

    @Override
    protected void init() {
        roleTypes.add(UserRoleType.USER);
        roleTypes.add(UserRoleType.ADMIN);
    }

    private void userExecute() throws Exception {
        System.out.println("[PROJECT LIST]");
        final User user = serviceLocator.getCurrentUser();
        final IProjectService projectService = serviceLocator.getProjectService();
        for (Project project : projectService.getProjectList(user.getId())) {
            System.out.println("PROJECT ID: " + project.getId() + " PROJECT NAME: " + project.getName());
        }
    }
}

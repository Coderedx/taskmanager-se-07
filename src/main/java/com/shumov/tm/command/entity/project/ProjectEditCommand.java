package com.shumov.tm.command.entity.project;

import com.shumov.tm.api.service.IProjectService;
import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.entity.User;
import com.shumov.tm.util.constant.UserRoleType;

public class ProjectEditCommand extends AbstractCommand {

    @Override
    public String command() {
        return "project-edit";
    }

    @Override
    public String getDescription() {
        return "Edit project name";
    }

    @Override
    public void execute() throws Exception {
        userExecute();
    }

    @Override
    protected void init() {
        roleTypes.add(UserRoleType.USER);
        roleTypes.add(UserRoleType.ADMIN);
    }

    private void userExecute() throws Exception {
        System.out.println("[EDIT PROJECT NAME BY ID]");
        System.out.println("ENTER PROJECT ID:");
        final String id = serviceLocator.getTerminalService().nextLine();
        final IProjectService projectService = serviceLocator.getProjectService();
        projectService.isWrongId(id);
        System.out.println("ENTER NEW PROJECT NAME:");
        final String name = serviceLocator.getTerminalService().nextLine();
        projectService.isWrongName(name);
        final User user = serviceLocator.getCurrentUser();
        projectService.editProjectNameById(user.getId(),id,name);
        System.out.println("PROJECT NAME HAS BEEN CHANGED SUCCESSFULLY");
    }
}

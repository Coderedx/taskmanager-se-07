package com.shumov.tm.command.entity.user;


import com.shumov.tm.api.service.IUserService;
import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.entity.User;
import com.shumov.tm.util.HashMd5;
import com.shumov.tm.util.constant.UserRoleType;

public class UserLoginCommand extends AbstractCommand {

    @Override
    public String command() {
        return "login";
    }

    @Override
    public String getDescription() {
        return "User login";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[USER LOGIN]");
        System.out.println("ENTER LOGIN:");
        final String login = serviceLocator.getTerminalService().nextLine();
        final IUserService userService = serviceLocator.getUserService();
        userService.isWrongLogin(login);
        System.out.println("ENTER PASSWORD:");
        final String pass = serviceLocator.getTerminalService().nextLine();
        userService.isWrongPass(pass);
        final User user = userService.getUser(login, HashMd5.getMd5(pass));
        System.out.println("authorization successful".toUpperCase());
        serviceLocator.setCurrentUser(user);
    }

    @Override
    protected void init() {
        roleTypes.add(UserRoleType.GUEST);
    }
}

package com.shumov.tm.command.entity.project;

import com.shumov.tm.api.service.IProjectService;
import com.shumov.tm.api.service.ITaskService;
import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.util.constant.UserRoleType;

public class ProjectAdminRemoveCommand extends AbstractCommand {

    @Override
    public String command() {
        return "project-remove-admin";
    }

    @Override
    public String getDescription() {
        return "Remove any project by id";
    }

    @Override
    public void execute() throws Exception {
        adminExecute();
    }

    @Override
    protected void init() {
        roleTypes.add(UserRoleType.ADMIN);
    }

    private void adminExecute() throws Exception {
        System.out.println("[REMOVE PROJECT BY ID]");
        System.out.println("ENTER PROJECT ID:");
        final String id = serviceLocator.getTerminalService().nextLine();
        final IProjectService projectService = serviceLocator.getProjectService();
        final ITaskService taskService = serviceLocator.getTaskService();
        projectService.isWrongId(id);
        projectService.removeProjectById(id, taskService);
        System.out.println("PROJECT HAS BEEN REMOVED SUCCESSFULLY");
    }
}

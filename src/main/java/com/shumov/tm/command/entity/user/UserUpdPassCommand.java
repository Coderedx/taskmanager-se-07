package com.shumov.tm.command.entity.user;

import com.shumov.tm.api.service.IUserService;
import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.entity.User;
import com.shumov.tm.util.constant.UserRoleType;


import java.util.Scanner;

public class UserUpdPassCommand extends AbstractCommand {

    @Override
    public String command() {
        return "user-pass";
    }

    @Override
    public String getDescription() {
        return "Set new password for current user";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[SET NEW PASSWORD]");
        System.out.println("ENTER NEW PASSWORD:");
        final String password = serviceLocator.getTerminalService().nextLine();
        final IUserService userService = serviceLocator.getUserService();
        userService.isWrongPass(password);
        final User user = serviceLocator.getCurrentUser();
        user.setPasswordHash(password);
        userService.mergeUser(user);
        System.out.println("Password changed successfully".toUpperCase());
    }

    @Override
    protected void init() {
        roleTypes.add(UserRoleType.ADMIN);
        roleTypes.add(UserRoleType.USER);
    }
}

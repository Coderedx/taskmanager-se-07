package com.shumov.tm.command.entity.project;

import com.shumov.tm.api.service.IProjectService;
import com.shumov.tm.api.service.ITaskService;
import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.entity.Task;
import com.shumov.tm.util.constant.UserRoleType;

public class ProjectAdminTasksCommand extends AbstractCommand {

    @Override
    public String command() {
        return "project-tasks-admin";
    }

    @Override
    public String getDescription() {
        return "Review all tasks";
    }

    @Override
    public void execute() throws Exception {
        adminExecute();
    }

    @Override
    protected void init() {
        roleTypes.add(UserRoleType.ADMIN);
    }

    private void adminExecute() throws Exception {
        System.out.println("[REVIEW PROJECT TASKS]");
        System.out.println("ENTER PROJECT ID:");
        final String id = serviceLocator.getTerminalService().nextLine();
        final IProjectService projectService = serviceLocator.getProjectService();
        projectService.isWrongId(id);
        projectService.getProject(id);
        System.out.println("[TASK LIST]");
        final ITaskService taskService = serviceLocator.getTaskService();
        for (Task task : taskService.getProjectTasks(id)) {
            System.out.println("TASK ID: " + task.getId() + "\nTASK NAME: " + task.getName()+
                    "\nUSER ID: "+ task.getOwnerId()+"\n");
        }
    }
}

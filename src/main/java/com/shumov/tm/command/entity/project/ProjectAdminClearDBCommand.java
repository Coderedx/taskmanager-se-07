package com.shumov.tm.command.entity.project;

import com.shumov.tm.api.service.IProjectService;
import com.shumov.tm.api.service.ITaskService;
import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.util.constant.UserRoleType;

public class ProjectAdminClearDBCommand extends AbstractCommand {

    @Override
    public String command() {
        return "project-clear-DB";
    }

    @Override
    public String getDescription() {
        return "Clear project DB";
    }

    @Override
    public void execute() throws Exception {
        adminExecute();
    }

    @Override
    protected void init() {
        roleTypes.add(UserRoleType.ADMIN);
    }

    private void adminExecute() throws Exception {
        System.out.println("[CLEAR DB PROJECTS]");
        System.out.println("[Enter \"Y\" if you confirm deletion of all projects]".toUpperCase());
        final String confirm = serviceLocator.getTerminalService().nextLine();
        if(!"y".equals(confirm.toLowerCase())){
            System.out.println("[operation has not been confirmed]".toUpperCase());
            return;
        }
        final IProjectService projectService = serviceLocator.getProjectService();
        final ITaskService taskService = serviceLocator.getTaskService();
        projectService.clearData(taskService);
        System.out.println("[ALL PROJECTS HAVE BEEN DELETED SUCCESSFULLY]");
    }
}

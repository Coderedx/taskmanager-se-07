package com.shumov.tm.command.entity.user;

import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.entity.User;
import com.shumov.tm.util.constant.UserRoleType;

public class UserAdminReviewCommand extends AbstractCommand {
    @Override
    public String command() {
        return "admin-review";
    }

    @Override
    public String getDescription() {
        return "All user review";
    }

    @Override
    public void execute() throws Exception {
        reviewAdmin();
    }

    @Override
    protected void init() {
        roleTypes.add(UserRoleType.ADMIN);
    }

    private void reviewAdmin() throws Exception {
        System.out.println("[USER REVIEW]");
        final User user = serviceLocator.getCurrentUser();
        System.out.println("Login: "+ user.getLogin());
        System.out.println("ID: "+ user.getId());
        System.out.println("Role: "+ user.getUserRoleType().toString());
        System.out.println("Description: "+ user.getDescription() +"\n");
    }
}

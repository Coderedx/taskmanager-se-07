package com.shumov.tm.command.entity.project;

import com.shumov.tm.api.service.IProjectService;
import com.shumov.tm.api.service.ITaskService;
import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.entity.Task;
import com.shumov.tm.entity.User;
import com.shumov.tm.util.constant.UserRoleType;

public class ProjectTasksCommand extends AbstractCommand {

    @Override
    public String command() {
        return "project-tasks";
    }

    @Override
    public String getDescription() {
        return "Review project tasks";
    }

    @Override
    public void execute() throws Exception {
        userExecute();
    }

    @Override
    protected void init() {
        roleTypes.add(UserRoleType.USER);
        roleTypes.add(UserRoleType.ADMIN);
    }

    private void userExecute() throws Exception {
        System.out.println("[REVIEW PROJECT TASKS]");
        System.out.println("ENTER PROJECT ID:");
        final String id = serviceLocator.getTerminalService().nextLine();
        final IProjectService projectService = serviceLocator.getProjectService();
        projectService.isWrongId(id);
        final User user = serviceLocator.getCurrentUser();
        projectService.getProject(user.getId(), id);
        System.out.println("[TASK LIST]");
        final ITaskService taskService = serviceLocator.getTaskService();
        for (Task task : taskService.getProjectTasks(user.getId(), id)) {
            System.out.println("TASK ID: " + task.getId() + " TASK NAME: " + task.getName());
        }
    }
}

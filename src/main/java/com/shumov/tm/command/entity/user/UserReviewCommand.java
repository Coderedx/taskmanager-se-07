package com.shumov.tm.command.entity.user;

import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.entity.User;
import com.shumov.tm.util.constant.UserRoleType;

public class UserReviewCommand extends AbstractCommand {

    @Override
    public String command() {
        return "user-review";
    }

    @Override
    public String getDescription() {
        return "User review";
    }

    @Override
    public void execute() throws Exception {
        reviewUser();
    }

    @Override
    protected void init() {
        roleTypes.add(UserRoleType.ADMIN);
        roleTypes.add(UserRoleType.USER);
    }

    private void reviewUser() throws Exception {
        System.out.println("[USER REVIEW]");
        final User user = serviceLocator.getCurrentUser();
        System.out.println("Login: "+ user.getLogin());
        System.out.println("Description: "+ user.getDescription() +"\n");
    }
}

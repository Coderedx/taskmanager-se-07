package com.shumov.tm.command.entity.task;

import com.shumov.tm.api.service.ITaskService;
import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.util.constant.UserRoleType;

public class TaskAdminClearDBCommand extends AbstractCommand {

    @Override
    public String command() {
        return "task-clear-admin";
    }

    @Override
    public String getDescription() {
        return "Clear task DB";
    }

    @Override
    public void execute() throws Exception {
        adminExecute();
    }

    @Override
    protected void init() {
        roleTypes.add(UserRoleType.ADMIN);
    }

    private void adminExecute() throws Exception {
        System.out.println("[CLEAR DB TASKS]");
        System.out.println("[Enter \"Y\" if you confirm deletion of all tasks]".toUpperCase());
        final String confirm = serviceLocator.getTerminalService().nextLine();
        if(!"y".equals(confirm.toLowerCase())){
            System.out.println("[operation has not been confirmed]".toUpperCase());
            return;
        }
        final ITaskService taskService = serviceLocator.getTaskService();
        taskService.clearData();
        System.out.println("[ALL TASKS HAVE BEEN DELETED SUCCESSFULLY]");
    }
}

package com.shumov.tm.command.entity.task;

import com.shumov.tm.api.service.IProjectService;
import com.shumov.tm.api.service.ITaskService;
import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.entity.Task;
import com.shumov.tm.util.constant.UserRoleType;

public class TaskAdminAddProjectCommand extends AbstractCommand {

    @Override
    public String command() {
        return "task-add-admin";
    }

    @Override
    public String getDescription() {
        return "Add task in any project";
    }

    @Override
    public void execute() throws Exception {
        adminExecute();
    }

    private void adminExecute() throws Exception {
        System.out.println("[ADD TASK IN PROJECT]");
        System.out.println("ENTER PROJECT ID:");
        final String projectId = serviceLocator.getTerminalService().nextLine();
        final IProjectService projectService = serviceLocator.getProjectService();
        projectService.isWrongId(projectId);
        projectService.getProject(projectId);
        System.out.println("ENTER TASK ID:");
        final String taskId = serviceLocator.getTerminalService().nextLine();
        final ITaskService taskService = serviceLocator.getTaskService();
        taskService.isWrongId(taskId);
        final Task task = taskService.getTask(taskId);
        task.setIdProject(projectId);
        taskService.addTaskInProject(taskId,task);
        System.out.println("TASK HAS BEEN ADD SUCCESSFULLY");
    }

    @Override
    protected void init() {
        roleTypes.add(UserRoleType.ADMIN);
    }
}

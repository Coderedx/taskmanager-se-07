package com.shumov.tm.command.entity.task;

import com.shumov.tm.api.service.IProjectService;
import com.shumov.tm.api.service.ITaskService;
import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.entity.Project;
import com.shumov.tm.entity.User;
import com.shumov.tm.util.constant.UserRoleType;

public class TaskCreateCommand extends AbstractCommand {

    @Override
    public String command() {
        return "task-create";
    }

    @Override
    public String getDescription() {
        return "Create new task";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK CREATE]\nENTER TASK NAME:");
        final String taskName = serviceLocator.getTerminalService().nextLine();
        final ITaskService taskService = serviceLocator.getTaskService();
        taskService.isWrongName(taskName);
        System.out.println("ENTER PROJECT ID:");
        final String id = serviceLocator.getTerminalService().nextLine();
        final IProjectService projectService = serviceLocator.getProjectService();
        projectService.isWrongId(id);
        final Project project = projectService.getProject(id);
        final User user = serviceLocator.getCurrentUser();
        taskService.createTask(user.getId(), taskName, project.getId());
        System.out.println("TASK HAS BEEN CREATED SUCCESSFULLY");
    }

    @Override
    protected void init() {
        roleTypes.add(UserRoleType.USER);
        roleTypes.add(UserRoleType.ADMIN);
    }
}

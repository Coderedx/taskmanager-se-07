package com.shumov.tm.command;

import com.shumov.tm.api.service.ServiceLocator;
import com.shumov.tm.util.constant.UserRoleType;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractCommand {

    public AbstractCommand() {
        init();
    }

    protected ServiceLocator serviceLocator;
    protected List<UserRoleType> roleTypes = new ArrayList<>();

    public abstract String command();

    public abstract String getDescription();

    public abstract void execute() throws Exception;

    public void setServiceLocator(ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    public List<UserRoleType> getRoleTypes() {
        return roleTypes;
    }

    public boolean isSecure() {
        final UserRoleType role = serviceLocator.getCurrentUser().getUserRoleType();
        return getRoleTypes().contains(role);
    }

    protected abstract void init();
}

package com.shumov.tm.service;

import com.shumov.tm.api.entity.Entity;
import com.shumov.tm.api.repository.IRepository;
import com.shumov.tm.api.service.Service;

import java.io.IOException;

public abstract class AbstractService<T extends Entity> implements Service {

    protected IRepository<T> repository;

    protected AbstractService() { }

    protected AbstractService(final IRepository<T> repository) {
        this.repository = repository;
    }

    @Override
    public final void isWrongName(final String name) throws Exception {
        if (name==null || name.isEmpty()){
            throw new IOException("WRONG NAME!");
        }
    }

    @Override
    public final void isWrongId(final String id) throws Exception{
        if(id==null || id.isEmpty()){
            throw new IOException("WRONG ID!");
        }
    }
}

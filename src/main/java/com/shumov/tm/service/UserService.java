package com.shumov.tm.service;

import com.shumov.tm.api.repository.IRepository;
import com.shumov.tm.entity.User;
import com.shumov.tm.util.constant.UserRoleType;

import java.io.IOException;

public class UserService extends AbstractService<User> implements com.shumov.tm.api.service.IUserService {

    public UserService(final IRepository<User> repository) {
        super(repository);
    }

    @Override
    public User getUser(final String login, final String password) throws Exception {
        return repository.findOne(password, login);
    }

    @Override
    public void mergeUser(final User user) throws Exception {
        repository.merge(user.getId(), user);
    }

    @Override
    public void createNewUser(final String login, final String password) throws Exception {
        final User user = new User(login, password);
        repository.persist(user);
    }

    @Override
    public void createNewUser(final String login, final String password, final UserRoleType userRole) throws Exception {
        final User user = new User(login, password, userRole);
        repository.persist(user);
    }

    @Override
    public void isWrongLogin(final String login) throws IOException {
        if (login==null || login.isEmpty()){
            throw new IOException("WRONG LOGIN!");
        }
    }

    @Override
    public void isWrongPass( final String pass) throws IOException{
        if(pass==null || pass.isEmpty()){
            throw new IOException("WRONG PASSWORD!");
        }
    }

    @Override
    public void isWrongDescription(final String description) throws IOException {
        if(description==null || description.isEmpty()){
            throw new IOException("WRONG DESCRIPTION!");
        }
    }
}

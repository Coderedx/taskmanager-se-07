package com.shumov.tm.service;

import com.shumov.tm.api.repository.IRepository;
import com.shumov.tm.api.service.IProjectService;
import com.shumov.tm.api.service.ITaskService;
import com.shumov.tm.entity.Project;
import com.shumov.tm.entity.Task;

import java.io.IOException;
import java.util.List;

public class ProjectService extends AbstractService<Project> implements IProjectService {

    public ProjectService(final IRepository<Project> repository) {
        super(repository);
    }

    @Override
    public void createProject(final String ownerId, final String name) throws Exception  {
        final Project project = new Project(name, ownerId);
        repository.persist(project);
    }

    @Override
    public List<Project> getProjectList() throws Exception {
        return repository.findAll();
    }

    @Override
    public List<Project> getProjectList(final String ownerId) throws Exception {
        return repository.findAll(ownerId);
    }

    @Override
    public Project getProject(final String id) throws Exception {
        return repository.findOne(id);
    }

    @Override
    public Project getProject(final String ownerId, final String id) throws Exception {
        return repository.findOne(ownerId, id);
    }

    @Override
    public void editProjectNameById(final String id, final String name) throws Exception {
        final Project project = repository.findOne(id);
        project.setName(name);
        repository.merge(id, project);
    }

    @Override
    public void editProjectNameById(final String ownerId, final String id, final String name) throws Exception {
        final Project project = repository.findOne(ownerId, id);
        project.setName(name);
        repository.merge(id, project);
    }

    @Override
    public void removeProjectById(final String id, final ITaskService taskService) throws Exception {
        repository.remove(id);
        final List<Task> list = taskService.getTaskList();
        for (Task task : list) {
            if(!list.isEmpty() && id.equals(task.getIdProject())){
                taskService.removeTaskById(task.getId());
            }
        }
    }

    @Override
    public void removeProjectById(final String ownerId, final String id, final ITaskService taskService)
            throws Exception {
        repository.remove(ownerId, id);
        for (Task task : taskService.getTaskList(ownerId)) {
            if(id.equals(task.getIdProject())){
                taskService.removeTaskById(ownerId, task.getId());
            }
        }
    }

    @Override
    public void clearData(final ITaskService taskService) throws Exception {
        repository.removeAll();
        taskService.clearData();
    }

    @Override
    public void clearData(final String ownerId, final ITaskService taskService) throws Exception {
        repository.removeAll(ownerId);
        taskService.clearData(ownerId);
    }
}

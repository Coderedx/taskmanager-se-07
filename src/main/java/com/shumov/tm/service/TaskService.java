package com.shumov.tm.service;

import com.shumov.tm.api.repository.IRepository;
import com.shumov.tm.api.service.ITaskService;
import com.shumov.tm.entity.Task;
import com.shumov.tm.exception.entity.EntityListIsEmptyException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class TaskService extends AbstractService<Task> implements ITaskService {

    public TaskService(final IRepository<Task> repository) {
        super(repository);
    }

    @Override
    public void createTask(final String ownerId, final String name, final String projectId) throws Exception {
        final Task task = new Task(ownerId, name, projectId);
        repository.persist(task);
    }

    @Override
    public List<Task> getTaskList() throws Exception {
        return repository.findAll();
    }

    @Override
    public List<Task> getTaskList(final String ownerId) throws Exception {
        return repository.findAll(ownerId);
    }

    @Override
    public Task getTask(final String id) throws Exception {
        return repository.findOne(id);
    }

    @Override
    public Task getTask(final String ownerId, final String id) throws Exception {
        return repository.findOne(ownerId, id);
    }

    @Override
    public List<Task> getProjectTasks(final String projectId) throws Exception {
        final List<Task> tasks = new ArrayList<>();
        for (Task task : repository.findAll()) {
            if(projectId.equals(task.getIdProject())){
                tasks.add(task);
            }
        }
        return tasks;
    }

    @Override
    public List<Task> getProjectTasks(final String ownerId, final String projectId) throws Exception {
        final List<Task> tasks = new ArrayList<>();
        for (Task task : repository.findAll(ownerId)) {
            if(projectId.equals(task.getIdProject())){
                tasks.add(task);
            }
        }
        return tasks;
    }

    @Override
    public void clearData() throws Exception {
        repository.removeAll();
    }

    @Override
    public void clearData(final String ownerId) throws Exception{
        repository.removeAll(ownerId);
    }

    @Override
    public void editTaskNameById(final String id, final String name) throws Exception {
        final Task task = repository.findOne(id);
        task.setName(name);
        repository.merge(id, task);
    }

    @Override
    public void editTaskNameById(final String ownerId, final String id, final String name) throws Exception {
        final Task task = repository.findOne(ownerId, id);
        task.setName(name);
        repository.merge(id, task);
    }

    @Override
    public void removeTaskById(final String id) throws Exception{
        repository.remove(id);
    }

    @Override
    public void removeTaskById(final String ownerId, final String id) throws Exception{
        repository.remove(ownerId, id);
    }

    @Override
    public void addTaskInProject(final String id, final Task task) throws Exception {
        repository.merge(id,task);
    }

    @Override
    public void addTaskInProject(final String ownerId, final String id, final Task task) throws Exception {
        repository.findOne(ownerId, id);
        repository.merge(id,task);
    }
}

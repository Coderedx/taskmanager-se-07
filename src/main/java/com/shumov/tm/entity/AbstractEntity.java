package com.shumov.tm.entity;

import com.shumov.tm.api.entity.Entity;

import java.util.UUID;

public class AbstractEntity implements Entity {

    protected String ownerId;
    protected String id = UUID.randomUUID().toString();

    public AbstractEntity() {

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }
}

package com.shumov.tm.entity;

import com.shumov.tm.api.entity.Entity;
import com.shumov.tm.util.HashMd5;
import com.shumov.tm.util.constant.UserRoleType;

public class User extends AbstractEntity implements Entity {

    private String login;
    private String passwordHash;
    private UserRoleType userRoleType;
    private String description = "some user";

    public User(String login, String password){
        this.login = login;
        this.passwordHash = HashMd5.getMd5(password);
        this.userRoleType = UserRoleType.USER;
    }

    public User(String login, String password, UserRoleType userRoleType) {
        this.login = login;
        this.passwordHash = HashMd5.getMd5(password);
        this.userRoleType = userRoleType;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    public void setPasswordHash(String password) {
        this.passwordHash = HashMd5.getMd5(password);
    }

    public UserRoleType getUserRoleType() {
        return userRoleType;
    }

    public void setUserRoleType(UserRoleType userRoleType) {
        this.userRoleType = userRoleType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}

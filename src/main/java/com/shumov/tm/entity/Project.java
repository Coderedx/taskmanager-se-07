package com.shumov.tm.entity;



import com.shumov.tm.api.entity.Entity;

import java.util.*;

public class Project extends AbstractEntity implements Entity {

    private String name = "Default Name";
    private String description = "Default Description";
    private Date dateStart;
    private Date dateFinish;

    public Project() {

    }

    public Project(String name, String ownerId){
        this.name = name;
        this.ownerId = ownerId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDateStart() {
        return dateStart;
    }

    public void setDateStart(Date dateStart) {
        this.dateStart = dateStart;
    }

    public Date getDateFinish() {
        return dateFinish;
    }

    public void setDateFinish(Date dateFinish) {
        this.dateFinish = dateFinish;
    }

}

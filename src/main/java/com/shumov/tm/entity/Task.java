package com.shumov.tm.entity;

import com.shumov.tm.api.entity.Entity;

import java.util.*;

public class Task extends AbstractEntity implements Entity {

    private String name = "Default Name";
    private String description = "Default Description";
    private String idProject = "No Project";
    private Date dateStart;
    private Date dateFinish;

    public Task(){

    }

    public Task(String ownerId, String name, String idProject){
        this.name = name;
        this.idProject = idProject;
        this.ownerId = ownerId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDateStart() {
        return dateStart;
    }

    public void setDateStart(Date dateStart) {
        this.dateStart = dateStart;
    }

    public Date getDateFinish() {
        return dateFinish;
    }

    public void setDateFinish(Date dateFinish) {
        this.dateFinish = dateFinish;
    }

    public String getIdProject() {
        return idProject;
    }

    public void setIdProject(String idProject) {
        this.idProject = idProject;
    }
}

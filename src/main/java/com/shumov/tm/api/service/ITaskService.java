package com.shumov.tm.api.service;

import com.shumov.tm.entity.Task;

import java.util.List;

public interface ITaskService extends Service {
    void createTask(String ownerId, String name, String projectId) throws Exception;

    List<Task> getTaskList() throws Exception;

    List<Task> getTaskList(String ownerId) throws Exception;

    Task getTask(String id) throws Exception;

    Task getTask(String ownerId, String id) throws Exception;

    List<Task> getProjectTasks(String projectId) throws Exception;

    List<Task> getProjectTasks(String ownerId, String projectId) throws Exception;

    void clearData() throws Exception;

    void clearData(String ownerId) throws Exception;

    void editTaskNameById(String id, String name) throws Exception;

    void editTaskNameById(String ownerId, String id, String name) throws Exception;

    void removeTaskById(String id) throws Exception;

    void removeTaskById(String ownerId, String id) throws Exception;

    void addTaskInProject(String id, Task task) throws Exception;

    void addTaskInProject(String ownerId, String id, Task task) throws Exception;
}

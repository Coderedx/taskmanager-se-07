package com.shumov.tm.api.service;

import com.shumov.tm.entity.User;
import com.shumov.tm.util.constant.UserRoleType;

import java.io.IOException;

public interface IUserService extends Service {
    User getUser(String login, String password) throws Exception;

    void mergeUser(User user) throws Exception;

    void createNewUser(String login, String password) throws Exception;

    void createNewUser(String login, String password, UserRoleType userRole) throws Exception;

    void isWrongLogin(String login) throws IOException;

    void isWrongPass(String pass) throws IOException;

    void isWrongDescription(String description) throws IOException;
}

package com.shumov.tm.api.service;

public interface Service {
    void isWrongName(String name) throws Exception;

    void isWrongId(String id) throws Exception;
}

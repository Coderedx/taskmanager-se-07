package com.shumov.tm.api.service;

import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.entity.User;
import com.shumov.tm.service.ProjectService;
import com.shumov.tm.service.TaskService;
import com.shumov.tm.service.UserService;

import java.util.List;
import java.util.Scanner;

public interface ServiceLocator {

    ITaskService getTaskService();
    IProjectService getProjectService();
    Scanner getTerminalService();
    List<AbstractCommand> getCommands();
    User getCurrentUser();
    User getGuestUser();
    IUserService getUserService();
    void setCurrentUser(User currentUser);

}

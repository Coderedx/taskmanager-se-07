package com.shumov.tm.api.service;

import com.shumov.tm.entity.Project;

import java.util.List;

public interface IProjectService extends Service {

    void createProject(String ownerId, String name) throws Exception;

    List<Project> getProjectList() throws Exception;

    List<Project> getProjectList(String ownerId) throws Exception;

    Project getProject(String id) throws Exception;

    Project getProject(String ownerId, String id) throws Exception;

    void editProjectNameById(String id, String name) throws Exception;

    void editProjectNameById(String ownerId, String id, String name) throws Exception;

    void removeProjectById(String id, ITaskService taskService) throws Exception;

    void removeProjectById(String ownerId, String id, ITaskService taskService) throws Exception;

    void clearData(ITaskService taskService) throws Exception;

    void clearData(String ownerId, ITaskService taskService) throws Exception;

}

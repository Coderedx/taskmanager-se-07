package com.shumov.tm.api.repository;

import com.shumov.tm.api.entity.Entity;

import java.util.List;

public interface IRepository<T extends Entity> {

    List<T> findAll() throws Exception;

    List<T> findAll(String ownerId) throws Exception;

    T findOne(String entityId) throws Exception;

    T findOne(String ownerId, String entityId) throws Exception;

    void persist(T entity) throws Exception;

    void merge(String entityId, T entity);

    void remove(String entityId) throws Exception;

    void remove(String ownerId, String entityId) throws Exception;

    void removeAll() throws Exception;

    void removeAll(String ownerId) throws Exception;
}

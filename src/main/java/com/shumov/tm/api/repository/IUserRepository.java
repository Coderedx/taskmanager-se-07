package com.shumov.tm.api.repository;

import com.shumov.tm.entity.User;

public interface IUserRepository extends IRepository<User> {

    @Override
    User findOne(String password, String login) throws Exception;

    @Override
    void persist(User user) throws Exception;
}

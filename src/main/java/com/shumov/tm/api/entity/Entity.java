package com.shumov.tm.api.entity;

import java.util.Date;

public interface Entity {

    String getId();
    void setId(String id);

    String getOwnerId();
    void setOwnerId(String ownerId);

}

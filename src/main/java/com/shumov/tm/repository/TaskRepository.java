package com.shumov.tm.repository;

import com.shumov.tm.api.repository.ITaskRepository;
import com.shumov.tm.entity.Task;


public class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {

}

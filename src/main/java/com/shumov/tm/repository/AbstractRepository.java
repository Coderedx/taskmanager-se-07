package com.shumov.tm.repository;

import com.shumov.tm.api.entity.Entity;
import com.shumov.tm.api.repository.IRepository;
import com.shumov.tm.exception.entity.EntityIsAlreadyExistException;
import com.shumov.tm.exception.entity.EntityListIsEmptyException;
import com.shumov.tm.exception.entity.EntityNotExistException;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public abstract class AbstractRepository<T extends Entity> implements IRepository<T> {

    protected final Map<String, T> entityMap = new LinkedHashMap<>();

    protected AbstractRepository() { }

    @Override
    public List<T> findAll() throws Exception {
        if(entityMap.isEmpty()) {
            return new ArrayList<>();
//            throw new EntityListIsEmptyException();
        } else {
            return new ArrayList<>(entityMap.values());
        }
    }

    @Override
    public List<T> findAll(final String ownerId) throws Exception {
        final List<T> list = new ArrayList<>();
        for (T entity: findAll()) {
            if(entity.getOwnerId().equals(ownerId)){
                list.add(entity);
            }
        }
        if(list.isEmpty()){
            return new ArrayList<>();
//            throw new EntityListIsEmptyException();
        } else {
            return list;
        }
    }

    @Override
    public T findOne(final String entityId) throws Exception {
        if(entityMap.isEmpty()){
            throw new EntityListIsEmptyException();
        } else if(!entityMap.containsKey(entityId)){
            throw new EntityNotExistException();
        } else {
            return entityMap.get(entityId);
        }
    }

    @Override
    public T findOne(final String ownerId, final String entityId) throws Exception {
        final T entity = findOne(entityId);
        if (entity.getOwnerId().equals(ownerId)){
            return entity;
        } else {
            throw new EntityNotExistException();
        }
    }

    @Override
    public void persist(final T entity) throws Exception {
        if(entityMap.containsKey(entity.getId())){
            throw new EntityIsAlreadyExistException();
        } else {
            entityMap.put(entity.getId(), entity);
        }

    }

    @Override
    public void merge(final String entityId, final T entity) {
        entity.setId(entityId);
        entityMap.put(entityId, entity);
    }

    @Override
    public void remove(final String entityId) throws Exception {
        if(entityMap.isEmpty()){
            throw new EntityListIsEmptyException();
        } else if(!entityMap.containsKey(entityId)){
            throw new EntityNotExistException();
        } else {
            entityMap.remove(entityId);
        }
    }

    @Override
    public void remove(final String ownerId, final String entityId) throws Exception {
        findOne(ownerId, entityId);
        remove(entityId);
    }

    @Override
    public void removeAll() throws Exception {
        if(entityMap.isEmpty()){
            throw new EntityListIsEmptyException();
        } else {
            entityMap.clear();
        }
    }

    @Override
    public void removeAll(final String ownerId) throws Exception {
        final List<T> list = findAll(ownerId);
        for (T entity : list) {
            remove(entity.getId());
        }
    }


}

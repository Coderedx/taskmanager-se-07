package com.shumov.tm.repository;


import com.shumov.tm.api.repository.IProjectRepository;
import com.shumov.tm.entity.Project;

public class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {

}

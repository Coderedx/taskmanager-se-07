package com.shumov.tm.repository;

import com.shumov.tm.api.repository.IUserRepository;
import com.shumov.tm.entity.User;
import com.shumov.tm.exception.entity.EntityIsAlreadyExistException;
import com.shumov.tm.exception.entity.EntityListIsEmptyException;
import com.shumov.tm.exception.entity.EntityNotExistException;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    public User findOne(final String password, final String login) throws Exception{
        if(entityMap.isEmpty()){
            throw new EntityListIsEmptyException();
        }
        for (User userDb : entityMap.values()){
            if (userDb.getLogin().equals(login) && userDb.getPasswordHash().equals(password)){
                return userDb;
            }
        }
        throw new EntityNotExistException();
    }

    @Override
    public void persist(final User user) throws Exception {
        if(entityMap.containsKey(user.getId())){
            throw new EntityIsAlreadyExistException();
        }
        for(User userDb : entityMap.values()){
            if(user.getLogin().equals(userDb.getLogin())){
                throw new EntityIsAlreadyExistException();
            }
        }
        entityMap.put(user.getId(), user);
    }
}
